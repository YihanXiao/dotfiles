# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
. /etc/bashrc
fi

#default aliases
#alias rm='rm -i'
alias ll="ls -lh"
alias v="vim"
alias sl=ls
alias mv="mv -i"           # -i prompts before overwrite
alias mkdir="mkdir -p"     # -p make parent dirs as needed
alias df="df -h"           # -h prints human readable format
alias la="ls -A"
alias lla="la -l"
alias analyze_aimd_d='analyze_aimd diffusivity Li+'
alias analyze_aimd_a='analyze_aimd arrhenius D_T.csv -p POSCAR -T 300 -s Li+'
alias analyze_aimd_ap='analyze_aimd arrhenius D_T.csv -p POSCAR -T 300 -s Li+ --plot'
alias dirs='dirs -v'
alias cs61c='ssh cs61c-hive'
alias rundocker='docker start -ai cs186'
alias ipy='ipython notebook'
alias ipy2='ipython2 notebook'
alias cp='cp -i'
alias mv='mv -i'
alias ll='ls -l'
alias openPOSCAR='open POSCAR -a vesta'
alias openCONTCAR='open CONTCAR -a vesta'
alias changeline='perl -i -pe "s/.*/6 12 18 72 1/ if $.==7" POSCAR'
alias checkenergy='grep 'entropy' OUTCAR |tail -1'
alias checkconverge='grep -r "reached re" `find . -name "OUTCAR"`'
alias xyz2='python2.7 /Users/yihanxiao/tsase/bin/xyz'
alias python='python3'
alias gs='git status '
alias ga='git add '
alias gaa='git add -A '
alias gb='git branch '
alias gc='git commit '
alias gcm='git commit -m '
alias go='git checkout '
alias gp='git push'
alias gl='git log --all --graph --decorate'

eval "$(fasd --init auto)"

# Color
export TERM="xterm-color"
export PS1='\[\e[0;33m\]\u\[\e[0m\]@\[\e[0;32m\]\h\[\e[0m\]:\[\e[0;34m\]\w\[\e[0m\]\$ '
export GREP_OPTIONS='--color=always'
export GREP_COLOR='1;35;40'
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# functions
mcd () {
    mkdir -p $1
    cd $1
}

#cl() {
#    local dir="$1"
#    local dir="${dir:=$HOME}"
#    if [[ -d "$dir" ]]; then
#        cd "$dir" >/dev/null; ls
#    else
#        echo "bash: cl: $dir: Directory not found"
#    fi


#for MD result analysis

# Download OUTCAR and CONTCAR from server
function rsvaspout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsvaspout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='vaspout.eps' --include='fe.dat' --include='XDATCAR' --include='OUTCAR' --include='CONTCAR' --include='vasprun.xml*' --exclude='*' ${1}@${2}:${3}/ ./$4
    fi
}

#function rsdefectout()
#{
#    if [ -z "$1" ]; then
#       echo "Usage: rsdefectout <usr_name@server_name:vasp_run_path>"
#    else
#       rsync -avz --include='*/' --include='POTCAR' --include='XDATCAR' --include='vasprun.xml' --include='CONTCAR' --exclude='*' ${1}@${2}:${3} ./$4
#    fi
#}
function rsnebout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsnebout <usr_name@server_name:neb_run_path>"
    else
       rsync -avz --include='*/' --include='POSCAR' --include='OUTCAR' --include='CONTCAR' --include='force.dat' --include='movie' --include='mep.eps' --include='neb.dat' --include='exts.dat' --include='spline.dat' --exclude='*' ${1}@${2}:${3} ./$4
    fi
}

function rsmdout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='XDATCAR' --include='OSZICAR' --include='table.dat' --include='summary*' --include='msd_*' --include='plot*' --include='CONTCAR' --include='vasprun.xml' --include='POTCAR' --include='INCAR' --exclude='*' ${1}@${2}:${3}/ ./$4
    fi
}

function rssimplemdout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='plot*' --include='vasprun.xml.gz' --include='CONTCAR'  --include='INCAR' --exclude='*' ${1}@${2}:${3}/ ./$4
    fi
}

function rsprobupload()
{
    if [ -z "$1" ]; then
       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='vasprun*' --exclude='*' ./ ${1}@${2}:${3}/
    fi
}

#function rsmdtableout()
#{
#    if [ -z "$1" ]; then
#       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
#    else
#       rsync -avz --include='*/' --include='summary*' --include='XDATCAR' --include='CONTCAR' --include='plot_nstart*' --include='table.dat' --exclude='*' ${1}@${2}:${3} ./$4
#    fi
#}
# No MP Parallelization (MKL)
export OMP_NUM_THREADS=1
export I_MPI_LINK='opt'
export MKL_NUM_THREADS=1
export MKL_DOMAIN_NUM_THREADS='MKL_BLAS=1'
export MKL_DYNAMIC='FALSE'
export OMP_DYNAMIC='FALSE'
export I_MPI_COMPATIBILITY=4

source /share/apps/intel/parallel_studio_xe_2015/bin/psxevars.sh intel64 &>/dev/null

#Binaries Path
#export PATH=/share/apps/bin:/home/yxiao/scripts:$PATH
#export PATH=/Users/yihanxiao/.local/bin:$PATH
export PATH=/Users/yihanxiao/Documents/codes:$PATH
export PATH=/Users/yihanxiao/Documents/codes/vtstscripts-933:$PATH
export PYTHONPATH=$HOME/tsase:$PYTHONPATH
export PYTHONPATH=$HOME/Documents/codes:$PYTHONPATH
export PATH=$HOME/tsase/bin:$PATH
export PATH=/Users/yihanxiao/mongodb/bin:$PATH
export PATH=/Users/yihanxiao/github/enumlib/src:$PATH
export PYTHONPATH=/Users/yihanxiao/github/enumlib/src:$PYTHONPATH
#for xyz
#export PYTHONPATH=/usr/local/lib/python2.7/site-packages:$PYTHONPATH

# Allow unlimited stack initialization (For Vasp)
# ulimit -s unlimited

#Intel compilers and MKL
# source /share/apps/intel/bin/compilervars.sh intel64

# User specific aliases and functions
alias webapp='cd /Users/yihanxiao/github/django_shangrila/materials_django'
alias pymatgen='cd /Users/yihanxiao/github/pymatgen/pymatgen'
alias pymatpro='cd /Users/yihanxiao/github/pymatpro/pymatpro'
alias pyabinitio='cd /Users/yihanxiao/github/pyabinitio/pyabinitio'
alias py27='source activate py27'
alias depy27='source deactivate py27'
alias matgen='ssh yxiao@128.3.18.35'

### NERSC Machines
alias cori="ssh -Y xiaoyh@cori"
alias nersc="ssh -i ~/.ssh/nersc -l xiaoyh cori.nersc.gov"
alias setup_nersc="~/Documents/software/sshproxy.sh -u xiaoyh"
#alias edison="ssh -Y xiaoyh@edison"

#### XSEDE Machines
alias stampede1="ssh -Y yihan@stamp1"
alias stampede="ssh -Y yihan@stampede2.tacc.utexas.edu"

#### LBL Ceder Group Machines
alias vega="ssh -Y yihanxiao@vega"
alias ginar="ssh -Y yxiao@ginar"
alias hginar="ssh -Y howard@ginar"

#### BRC Clusters
alias savio="ssh yihanxiao@hpc.brc.berkeley.edu"

#### Lawrencium clusters
alias law="ssh yihanxiao@lrc-login.lbl.gov"
export LD_LIBRARY_PATH=~/.mujoco/mjpro150/bin/

export EDITOR=/usr/bin/vim
set -o vi
