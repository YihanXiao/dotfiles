# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/Users/yihanxiao/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#default aliases
#alias rm='rm -i'
alias ll="ls -lh"
alias v="vim"
alias sl=ls
alias up='cd ..'
alias mv="mv -i"           # -i prompts before overwrite
alias mkdir="mkdir -p"     # -p make parent dirs as needed
alias df="df -h"           # -h prints human readable format
alias la="ls -A"
alias lla="la -l"
alias analyze_aimd_d='analyze_aimd diffusivity Li+'
alias analyze_aimd_a='analyze_aimd arrhenius D_T.csv -p POSCAR -T 300 -s Li+'
alias analyze_aimd_ap='analyze_aimd arrhenius D_T.csv -p POSCAR -T 300 -s Li+ --plot'
alias dirs='dirs -v'
alias cs61c='ssh cs61c-hive'
alias rundocker='docker start -ai cs186'
alias ipy='ipython notebook'
alias ipy2='ipython2 notebook'
alias cp='cp -i'
alias mv='mv -i'
alias ll='ls -l'
alias openPOSCAR='open POSCAR -a vesta'
alias openCONTCAR='open CONTCAR -a vesta'
alias changeline='perl -i -pe "s/.*/6 12 18 72 1/ if $.==7" POSCAR'
alias checkenergy='grep 'entropy' OUTCAR |tail -1'
alias checkconverge='grep -r "reached re" `find . -name "OUTCAR"`'
alias xyz2='python2.7 /Users/yihanxiao/tsase/bin/xyz'
alias python='python3'
alias gs='git status '
alias ga='git add '
alias gaa='git add -A '
alias gb='git branch '
alias gc='git commit '
alias gcm='git commit -m '
alias go='git checkout '
alias gp='git push'
alias glog='git log --all --graph --decorate'

# functions
mcd () {
    mkdir -p $1
    cd $1
}
# Download OUTCAR and CONTCAR from server
function rsvaspout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsvaspout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='vaspout.eps' --include='fe.dat' --include='XDATCAR' --include='OUTCAR' --include='CONTCAR' --include='vasprun.xml*' --exclude='*' ${1}@${2}:${3}/ ./$4
    fi
}

#function rsdefectout()
#{
#    if [ -z "$1" ]; then
#       echo "Usage: rsdefectout <usr_name@server_name:vasp_run_path>"
#    else
#       rsync -avz --include='*/' --include='POTCAR' --include='XDATCAR' --include='vasprun.xml' --include='CONTCAR' --exclude='*' ${1}@${2}:${3} ./$4
#    fi
#}
function rsnebout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsnebout <usr_name@server_name:neb_run_path>"
    else
       rsync -avz --include='*/' --include='POSCAR' --include='OUTCAR' --include='CONTCAR' --include='force.dat' --include='movie' --include='mep.eps' --include='neb*.dat' --include='exts.dat' --include='spline.dat' --exclude='*' ${1}@${2}:${3} ./$4
    fi
}

function rsmdout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='XDATCAR' --include='OSZICAR' --include='table.dat' --include='summary*' --include='msd_*' --include='plot*' --include='CONTCAR' --include='vasprun.xml' --include='POTCAR' --include='INCAR' --exclude='*' ${1}@${2}:${3}/ ./$4
    fi
}

function rssimplemdout()
{
    if [ -z "$1" ]; then
       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='plot*' --include='vasprun.xml.gz' --include='CONTCAR'  --include='INCAR' --exclude='*' ${1}@${2}:${3}/ ./$4
    fi
}

function rsprobupload()
{
    if [ -z "$1" ]; then
       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
    else
       rsync -avz --include='*/' --include='vasprun*' --exclude='*' ./ ${1}@${2}:${3}/
    fi
}

#function rsmdtableout()
#{
#    if [ -z "$1" ]; then
#       echo "Usage: rsmdout <usr_name@server_name:vasp_run_path>"
#    else
#       rsync -avz --include='*/' --include='summary*' --include='XDATCAR' --include='CONTCAR' --include='plot_nstart*' --include='table.dat' --exclude='*' ${1}@${2}:${3} ./$4
#    fi
#}

#Binaries Path
#export PATH=/share/apps/bin:/home/yxiao/scripts:$PATH
#export PATH=/Users/yihanxiao/.local/bin:$PATH
export PATH=/Users/yihanxiao/Documents/codes:$PATH
export PATH=/Users/yihanxiao/Documents/codes/vtstscripts-933:$PATH
export PATH=$HOME/tsase/bin:$PATH
export PATH=$HOME/miniconda3/bin:$PATH
export PATH=/Users/yihanxiao/mongodb/bin:$PATH
export PATH=/Users/yihanxiao/github/enumlib/src:$PATH

export PYTHONPATH=/Users/yihanxiao/github/enumlib/src:$PYTHONPATH
export PYTHONPATH=$HOME/tsase:$PYTHONPATH
export PYTHONPATH=$HOME/Documents/codes:$PYTHONPATH
export PATH=/usr/local/bin/:$PATH
#for xyz
#export PYTHONPATH=/usr/local/lib/python2.7/site-packages:$PYTHONPATH
#
# User specific aliases and functions
alias webapp='cd /Users/yihanxiao/github/django_shangrila/materials_django'
alias pymatgen='cd /Users/yihanxiao/github/pymatgen/pymatgen'
alias pymatpro='cd /Users/yihanxiao/github/pymatpro/pymatpro'
alias pyabinitio='cd /Users/yihanxiao/github/pyabinitio/pyabinitio'
alias py27='source activate py27'
alias depy27='source deactivate py27'
alias matgen='ssh yxiao@128.3.18.35'

### NERSC Machines
alias cori="ssh -Y xiaoyh@cori"
alias nersc="ssh -i ~/.ssh/nersc -l xiaoyh cori.nersc.gov"
alias setup_nersc="~/Documents/software/sshproxy.sh -u xiaoyh"
#alias edison="ssh -Y xiaoyh@edison"

#### XSEDE Machines
alias stampede1="ssh -Y yihan@stamp1"
alias stampede="ssh -Y yihan@stampede2.tacc.utexas.edu"

#### LBL Ceder Group Machines
alias vega="ssh -Y yihanxiao@vega"
alias ginar="ssh -Y yxiao@ginar"
alias hginar="ssh -Y howard@ginar"

#### BRC Clusters
alias savio="ssh yihanxiao@hpc.brc.berkeley.edu"

#### Lawrencium clusters
alias law="ssh yihanxiao@lrc-login.lbl.gov"
export LD_LIBRARY_PATH=~/.mujoco/mjpro150/bin/

# Color
#export PS1='%n@%m %~$ '
#autoload -U promptinit && promptinit
export PS1="%{%F{red}%}%n%{%f%}@%{%F{blue}%}%m %{%F{yellow}%}%~%{$%f%}%$ "
bindkey -v
bindkey '^R' history-incremental-search-backward
bindkey -e
export EDITOR=/usr/bin/vim
set -o vi
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
